% Momentum equations 
j = 1;
jsi = j;
vi1 = 2; vsi1 = vi1 + juncs; 
vi2 = 3; vsi2 = vi2 + juncs;
dRdU(jsi,jsi) = denJ(j)*Area;
dRdU(jsi,vsi1) = ((2*dx(vi1)+dx(vi2))/(dx(vi1)+dx(vi2)))...
               * U(jsi)* Area*dddp(vi1);
dRdU(jsi,vsi2) = ((-dx(vi1))/(dx(vi1)+dx(vi2)))...
               * U(jsi)* Area*dddp(vi2);       
for j = 2:(juncs-1)
    jsi = j  ;
    ivi = j  ; ivsi = ivi+juncs; 
    ovi = j+1; ovsi = ovi+juncs; 
    uji = j-1; ujsi = uji;
    dji = j+1; djsi = dji;   
    
    uT       = 0.5*(U(ujsi)+U(jsi));
    Fiv      = den(ivi)*uT*Area;
    dFivdu   = 0.5*den(ivi)*Area; % dFiv/duuj
    dFivduuj = dFivdu;            % dFiv/du
    dFivdpiv = uT*Area*dddp(ivi);
    
    uT       = 0.5*(U(jsi) + U(djsi));
    Fov      = den(ovi)*uT*Area;
    dFovdu   = 0.5*den(ovi)*Area;
    dFovdudj = dFovdu;
    dFovdpov = uT*Area*dddp(ovi);
    
    Loss      = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*denJ(j)*abs(U(jsi));   
    dLossdu   = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*denJ(j)*(1+dfdReN(j));
    dLossdpiv = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*abs(U(jsi))*(1+dfdReN(j))...
              * (dx(ovi)/(dx(ivi)+dx(ovi)))*dddp(ivi);
    dLossdpov = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*abs(U(jsi))*(1+dfdReN(j))...
              * (dx(ivi)/(dx(ivi)+dx(ovi)))*dddp(ovi);          
    
    dRdU(jsi,ujsi) = -(Fiv + U(ujsi)*dFivduuj);
    dRdU(jsi,jsi)  = -U(ujsi)*dFivdu + Fov + Loss ...
                   + (dFovdu + dLossdu)*U(jsi);
    dRdU(jsi,djsi) = U(jsi)*dFovdudj;
    dRdU(jsi,ivsi) = -Area - U(ujsi)*dFivdpiv + U(jsi)*dLossdpiv;
    dRdU(jsi,ovsi) = Area + U(jsi)*(dFovdpov + dLossdpov);
end
j = juncs;
jsi = j  ; 
ivi = j  ; ivsi = ivi+juncs;
ovi = j+1; ovsi = ovi+juncs;
uji = j-1; ujsi = uji;     

uT       = 0.5*(U(ujsi) + U(jsi));
Fiv      = den(ivi)*uT*Area;
dFivdu   = 0.5*den(ivi)*Area; % dFiv/duuj
dFivduuj = dFivdu;            % dFiv/du
dFivdpiv = uT*Area*dddp(ivi);

uT       = U(jsi);
Fov      = den(ovi)*uT*Area;
dFovdu   = den(ovi)*Area;
dFovdpov = uT*Area*dddp(ovi);

Loss      = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*denJ(j)*abs(U(jsi));  
dLossdu   = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*denJ(j)*(1+dfdReN(j));
dLossdpiv = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*abs(U(jsi))*(1+dfdReN(j))...
          * (dx(ovi)/(dx(ivi)+dx(ovi)))*dddp(ivi);
dLossdpov = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*abs(U(jsi))*(1+dfdReN(j))...
          * (dx(ivi)/(dx(ivi)+dx(ovi)))*dddp(ovi);  

dRdU(jsi,ujsi) = -(Fiv + U(ujsi)*dFivduuj);
dRdU(jsi,jsi)  = -U(ujsi)*dFivdu + Fov + Loss ...
               + (dFovdu + dLossdu)*U(jsi);
dRdU(jsi,ivsi) = -Area - U(ujsi)*dFivdpiv + U(jsi)*dLossdpiv;
dRdU(jsi,ovsi) = Area + U(jsi)*(dFovdpov + dLossdpov);
    
% Continuity Equations
j = 1        ; 
vsi = j+juncs;
dRdU(vsi,vsi) = 1.0;
j = 2        ;
vsi = j+juncs;
ovi = j+1    ; ovsi = ovi+juncs;
iji = j-1    ; ijsi = iji;
oji = j      ; ojsi = oji;
dRdU(vsi,ijsi) = -denJ(iji)*Area;
dRdU(vsi,ojsi) = denJ(oji)*Area;
dRdU(vsi,vsi)  = (U(ojsi)*Area*(dx(ovi)/(dx(j)+dx(ovi)))...
                 -U(ijsi)*Area*((2*dx(j)+dx(ovi))/(dx(j)+dx(ovi))))...
                 *dddp(j);
dRdU(vsi,ovsi) = (U(ojsi)*Area + U(ijsi)*Area)*(dx(j)/(dx(j)+dx(ovi)))...
                 *dddp(ovi);
for j=3:(vols-1)
    vsi = j+juncs;
    ivi = j-1    ; ivsi = ivi+juncs;
    ovi = j+1    ; ovsi = ovi+juncs;
    iji = j-1    ; ijsi = iji;
    oji = j      ; ojsi = oji;
    dRdU(vsi,ijsi) = -denJ(iji)*Area;
    dRdU(vsi,ojsi) = denJ(oji)*Area;
    dRdU(vsi,ivsi)  = -U(ijsi)*Area*(dx(j)/(dx(j)+dx(ivi)))*dddp(ivi);
    dRdU(vsi,vsi)  = (U(ojsi)*Area*(dx(ovi)/(dx(j)+dx(ovi)))...
                     -U(ijsi)*Area*(dx(ivi)/(dx(j)+dx(ivi))))...
                     *dddp(j);
    dRdU(vsi,ovsi) = U(ojsi)*Area*(dx(j)/(dx(j)+dx(ovi)))*dddp(ovi);
end
j = vols;
vsi = j+juncs;
dRdU(vsi,vsi) = 1.0;



