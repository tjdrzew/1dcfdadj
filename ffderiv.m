function dfdRe = ffderiv(Reyn,roughness)

    alfa = (8/Reyn)^12;
    dalfadRe = -(12/Reyn)*alfa;
    s = (7/Reyn)^0.9 + 0.27*roughness;
    A = (-2.457*log(s))^16;
    dAdRe = (16*2.457/s)*((-2.457*log(s))^15)*0.9*(7^0.9)*(Reyn^-1.9);
    B = (37530/Reyn)^16;
    dBdRe = -16*B/Reyn;
    beta = (A+B)^-1.5;
    dbetadRe = -1.5*((A+B)^-2.5)*(dAdRe + dBdRe);
    dfdRe = (2/3)*((alfa+beta)^(-11/12))*(dalfadRe+dbetadRe);