% Update thermo-physical properties
if fluid == 1
   for j=1:vols
       den(j) = p(j)/(287.0*T);   
       visc(j) = (1.458E-6)*(T^1.5)/(T+110.4);       
   end
elseif fluid == 2
    for j=1:vols
        den(j) = 1000.0;
        visc(j) = 2.0E-5;
    end
end

denJ(1) = ((2*dx(2)+dx(3))*den(2)-dx(2)*den(3))/(dx(2)+dx(3));
viscT   = visc(2)+(visc(2)-visc(3))*dx(2)/(dx(2)+dx(3));
Reyn(1) = denJ(1)*u(1)*D/viscT;
FDarcyJ(1) = frictionFactor(Reyn(1),0.0);
for j=2:juncs
    ivi = j;  ovi = j+1;
    denJ(j) = interpVar(den(ivi),den(ovi),dx(ivi),dx(ovi));
    viscT = interpVar(visc(ivi),visc(ovi),dx(ivi),dx(ovi));
    Reyn(j) = denJ(j)*u(j)*D/viscT;
    FDarcyJ(j)=frictionFactor(Reyn(j),0.0);
end

% Update momentum coefficient matrix 
Au(1,1)=denJ(1)*Area;
for j=2:(juncs-1)
    ivi = j; ovi = j+1;
 
    iji = j-1; oji = j;
    uT = 0.5*(u(iji) + u(oji));
    Fiv = den(ivi)*uT*Area;
 
    iji = j; oji = j+1;
    uT = 0.5*(u(iji) + u(oji));
    Fov = den(ovi)*uT*Area;
  
    Loss = 0.5*FDarcyJ(j)*(dxJ(j)/D)*denJ(j)*abs(u(j))*Area;
    
    Au(j,j-1) = -Fiv;
    Au(j,j) = Fov + Loss;
    if solver == 1
        du(j) = Area/(Au(j,j)-Au(j,j-1));
    else
        du(j) = Area/Au(j,j); 
    end
end
j=juncs;
ivi = j; ovi = j+1;
 
iji = j-1; oji = j;
uT = 0.5*(u(iji) + u(oji));
Fiv = den(ivi)*uT*Area;
 
iji = j; uT = u(iji);
Fov = den(ovi)*uT*Area;
 
Loss = 0.5*FDarcyJ(j)*(dxJ(j)/D)*denJ(j)*abs(u(j))*Area;    
Au(j,j-1) = -Fiv;
Au(j,j) = Fov + Loss;
if solver == 1;
    du(j) = Area/(Au(j,j)-Au(j,j-1));
else
    du(j) = Area/Au(j,j); 
end

% Update pressure correction equation
Ap(1,1)=1.0;
for j=2:(vols-1)
    iji=j-1; oji=j;     
    ain = denJ(iji)*du(iji)*Area;  
    aout = denJ(oji)*du(oji)*Area;
    
    Ap(j,j-1) = -ain;
    Ap(j,j+1) = -aout;
    Ap(j,j) = ain + aout; 
end
j=vols; Ap(j,j)=1.0;

% Set up momentum source vector and solve
bu(1) = W;
for j=2:juncs
    ivi = j; ovi = j+1;
    bu(j) = (p(ivi)-p(ovi))*Area;  
end
ug = Au\bu;