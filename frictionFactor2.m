function [f,dfdReN]=frictionFactor2(Reyn,roughness)
    
    L = (8/Reyn)^12;
    E = (7/Reyn)^0.9 + 0.27*roughness;
    D = -2.457*log(E);
    A = D^16;
    B = (37530.0/Reyn)^16;
    T = (A+B)^(-3/2);
    f = 8.0*(L+T)^(1/12);
    dLdRe = -12*L/Reyn;
    dAdD  = 16*A/D;
    dDdE  = -2.457/E;
    dEdRe = -0.9*(E-0.27*roughness)/Reyn;
    dAdRe = dAdD*dDdE*dEdRe;
    dBdRe = -16*B/Reyn;
    dTdRe = -1.5*(dAdRe + dBdRe)*T/(A+B);
    dfdReN = Reyn*(dLdRe + dTdRe)/(12*(L+T));
    %dfdReN = (Re/f)*(df/dRe)