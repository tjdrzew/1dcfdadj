function varT = interpVar(iviVar,oviVar,iviDx,oviDx)

    varT = iviVar + iviDx*(oviVar-iviVar)/(iviDx+oviDx);   
end