% Set up pressure correction source vector and solve
bp(1)=0.0;
for j=2:(vols-1)
    iji=j-1; oji=j;   
    bp(j) = denJ(iji)*ug(iji)*Area - denJ(oji)*ug(oji)*Area;
end
bp(vols)=0.0;
pc = Ap\bp;

% Correct velocity and pressure fields
for j=1:juncs
    ivi = j; ovi = j+1;
    uc(j) = (pc(ivi) - pc(ovi))*du(j);
end
p = p + prlx*pc;
u = (1-urlx)*u + urlx*(ug+uc); 