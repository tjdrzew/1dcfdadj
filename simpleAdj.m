%% Allocate Memory

vols = pvols + 2;
juncs = vols - 1;
dx = zeros(vols,1);
dxJ = zeros(juncs,1);
u=zeros(juncs,1);
ug=zeros(juncs,1);
uc=zeros(juncs,1);
p=zeros(vols,1);
pc=zeros(vols,1);
den = zeros(vols,1);
denJ = zeros(juncs,1);
visc = zeros(vols,1);
Reyn = zeros(juncs,1);
FDarcyJ = zeros(juncs,1);
Au = zeros(juncs);
bu=zeros(juncs,1);
du=zeros(juncs,1);
Ap = zeros(vols);
bp = zeros(vols,1);

%% Initialize Field Variables

Area = 0.025*pi*D;

dx(1) = 0;
for j=2:vols
   dx(j) =  L/pvols;
end
dx(vols) = 0;
for j=1:juncs
   ivi = j; ovi = j+1;
   dxJ(j) = 0.5*(dx(ivi) + dx(ovi));
end

p(1) = pin;
for j = 2:vols
    p(j) = pout;   
end
if fluid == 1
    denT = (dx(2)*p(1)+dx(1)*p(2))/(287.0*T)/(dx(1)+dx(2));
elseif fluid == 2
    denT = 1000;
end
for j=1:juncs
    u(j) = W/denT/Area; 
end
updateMatrices

%% Solve
if solver == 2
    fprintf('\nPreconditioning ...');
    fprintf('\n%s\t%s\t\t%s\n','it','mom','cont');   
    for iteration = 1:preIt
        updateFieldVars
        updateMatrices
        segResid
        fprintf('%i\t%8.4e\t%8.4e\n',iteration,residu,residc);
    end
    % clear neaded variables no longer needed and set up new variables  
    clear ug uc pc Au bu du Ap bp 
    U = zeros(juncs+vols,1);
    U(1:juncs) = u;
    U((juncs+1):(juncs+vols))=p;    
    clear u p
    R = zeros(juncs+vols,1);
    U0 = zeros(juncs+vols,1);
    dU = zeros(juncs+vols,1);
    dRdU = zeros(juncs+vols);
    dddp = zeros(vols,1);
    dfdReN = zeros(juncs,1);
    fprintf('\nCalculation ...');
    fprintf('\n%s\t%s\t\t%s\t\t%s\n','it','mom','cont','relax'); 
    iterate = true; iteration = 0;
    while (iterate)    
        iteration = iteration + 1;
        sysResid    
        sysJacobian
        dU = -dRdU\R;    
        upd = true; relax = 1.0; U0=U; residu0=residu; residc0=residc;
        while (upd)
            U = U0 + relax*dU;
            sysResid
            if ((residu < residu0)&&(residc < residc0))
                upd = false;
            else
                relax = 0.5*relax;
            end
        end
        fprintf('%i\t%8.4e\t%8.4e\t%4.2e\n',iteration,residu,residc,relax);
        if (residu < utol && residc < ctol)
            iterate = false;
        elseif (iteration >= maxit)
            iterate = false;
        end      
    end
else
    fprintf('\n%s\t%s\t\t%s\n','it','mom','cont');
    iterate = true; iteration = 0;
    while (iterate)
        iteration = iteration + 1;   
        updateFieldVars
        updateMatrices
        segResid
        fprintf('%i\t%8.4e\t%8.4e\n',iteration,residu,residc);
        if (residu < utol && residc < ctol)
            iterate = false;
        elseif (iteration >= maxit)
            iterate = false;
        end
    end
end
