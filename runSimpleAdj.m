%% Problem Parameters
clear all; close all; clc

pvols = 10;
L = 1.5;
D = 0.076;
W = 15.5;
pin = 101.3E3;
pout = 101.3E3;
T = 300.0; 
urlx = 0.3; prlx = 0.1;
utol = 1.0E-9; ctol = 1.0E-9; maxit = 300;
fluid = 2; % 1 for air, 2 for water
solver = 0; % Use 0 for SIMPLE, 1 for SIMPLEC, 2 for Newton
preIt = 1;

simpleAdj
if solver == 2
    dRdW = zeros(juncs+vols,1); dRdW(1) = -1;
    dUdW = -dRdU\dRdW;
end
W0 = W;

%% Plot Output

x=zeros(vols-1,1);
x(1)=dx(2)/2;
for j=2:(vols-1);
    x(j)=x(j-1)+(dx(j)+dx(j+1))/2;
end
xJ=zeros(juncs,1);
for j=2:juncs;
    xJ(j) = xJ(j-1) + dx(j); 
end
scrsz = get(0,'ScreenSize');

p = U((juncs+2):(juncs+vols));
u = U(1:juncs);

figure('Name','Pressure','Position', ...
       [1 scrsz(4)/2 scrsz(3)/2.3 scrsz(4)/2.3]) 
plot(x,p)
figure('Name','Velocity','Position', ...
       [scrsz(3)/2 scrsz(4)/2 scrsz(3)/2.3 scrsz(4)/2.3]) 
plot(xJ,u)

%% Finite Difference Check (Average Velocity)

dJdU1 = zeros(1,juncs+vols); dJdU1(1:juncs) = 1.0/juncs;
dJdW1 = dJdU1*dUdW;

Jr = mean(U(1:juncs));
JN = zeros(6,1);
dJdWN = zeros(6,1);
err = zeros(6,1);
solver = 0;
for k=1:6
    dW = W0*10^-k;
    W = W0 + dW;
    simpleAdj
    JN(k) = mean(u);
    dJdWN(k) = (JN(k) - Jr)/dW;
    err(k) = (dJdWN(k) -dJdW1)/dJdW1;
end

%% Finite Difference Check (Average Pressure)

dJdU2 = zeros(1,juncs+vols); dJdU2((juncs+1):(juncs+vols)) = 1.0/vols;
dJdW2 = dJdU2*dUdW;

Jr = mean(U((juncs+1):(juncs+vols)));
JN = zeros(6,1);
dJdWN = zeros(6,1);
err = zeros(6,1);
solver = 0;
for k=1:6
    dW = W0*10^-k;
    W = W0 + dW;
    simpleAdj
    JN(k) = mean(p);
    dJdWN(k) = (JN(k) - Jr)/dW;
    err(k) = (dJdWN(k) -dJdW2)/dJdW2;
end