% Update thermo-physical properties
if fluid == 1
   for j=1:vols
       pT = U(j+juncs);
       den(j) = pT/(287.0*T);
       dddp(j) = 1/(287.0*T);
       visc(j) = (1.458E-6)*(T^1.5)/(T+110.4);       
   end
elseif fluid == 2
    for j=1:vols
        den(j) = 1000.0;
        dddp(j) = 0.0;
        visc(j) = 2.0E-5;
    end
end

denJ(1) = ((2*dx(2)+dx(3))*den(2)-dx(2)*den(3))/(dx(2)+dx(3));
viscT   = visc(2)+(visc(2)-visc(3))*dx(2)/(dx(2)+dx(3));
Reyn(1) = denJ(1)*U(1)*D/viscT;
[FDarcyJ(1),dfdReN(1)] = frictionFactor2(Reyn(1),0.0);
for j=2:juncs
    ivi = j;  ovi = j+1;
    denJ(j) = interpVar(den(ivi),den(ovi),dx(ivi),dx(ovi));
    viscT = interpVar(visc(ivi),visc(ovi),dx(ivi),dx(ovi));
    Reyn(j) = denJ(j)*U(j)*D/viscT;
    [FDarcyJ(j),dfdReN(j)]=frictionFactor2(Reyn(j),0.0);
end

% Set up momentum residuals
numerator = 0.0; denominator = 0.0;
j = 1;
jsi = j;
vi1 = 2; vsi1 = vi1 + juncs; 
vi2 = 3; vsi2 = vi2 + juncs;
R(jsi) = denJ(j)*U(jsi)*Area - W;
for j = 2:(juncs-1)
    jsi = j  ;
    ivi = j  ; ivsi = ivi+juncs; 
    ovi = j+1; ovsi = ovi+juncs; 
    iji = j-1; ijsi = iji;
    oji = j+1; ojsi = oji;      
    
    uT       = 0.5*(U(ijsi)+U(jsi));
    Fiv      = den(ivi)*uT*Area;   
    
    uT       = 0.5*(U(jsi) + U(ojsi));
    Fov      = den(ovi)*uT*Area;
    
    Loss      = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*denJ(j)*abs(U(jsi));     
        
    R(jsi)=(U(ovsi)-U(ivsi))*Area -Fiv*U(ijsi) + (Fov+Loss)*U(jsi);     
    numerator = numerator + abs(R(jsi));
    denominator = denominator + (Fov + Loss)*U(jsi);
end
j=juncs  ;
jsi = j  ;
ivi = j  ; ivsi = ivi+juncs; 
ovi = j+1; ovsi = ovi+juncs; 
iji = j-1; ijsi = iji;
oji = j+1; ojsi = oji;      
    
uT       = 0.5*(U(ijsi)+U(jsi));
Fiv      = den(ivi)*uT*Area;   
    
uT       = U(jsi);
Fov      = den(ovi)*uT*Area;
    
Loss      = 0.5*FDarcyJ(j)*(dxJ(j)/D)*Area*denJ(j)*abs(U(jsi));   
     
R(jsi)=(U(ovsi)-U(ivsi))*Area -Fiv*U(ijsi) + (Fov+Loss)*U(jsi); 
numerator = numerator + abs(R(jsi));
denominator = denominator + (Fov + Loss)*U(jsi);
residu = numerator/denominator;

% Set up continuity residuals
residc = 0.0;
j = 1        ; 
vsi = j+juncs;
R(vsi) = U(vsi) - pin; 
for j=2:(vols-1)
    vsi = j+juncs;
    uvi = j-1    ; uvsi = uvi+juncs;
    dvi = j+1    ; dvsi = dvi+juncs;
    iji = j-1    ; ijsi = iji;
    oji = j      ; ojsi = oji;
    R(vsi) = denJ(oji)*U(ojsi)*Area - denJ(iji)*U(ijsi)*Area;
    residc = residc + abs(R(vsi));
end
j=vols;
vsi = j+juncs;
R(vsi) = U(vsi)-pout;